#!/usr/bin/env bash

python3 -m venv venv
source venv/bin/activate
	python3 -m pip --quiet install --upgrade pip
	python3 -m pip --quiet install --upgrade wheel
	python3 -m pip --quiet install --upgrade --requirement requirements.txt
deactivate
