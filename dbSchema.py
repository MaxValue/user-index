#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sqlalchemy as sqla
from sqlalchemy.ext.declarative import declarative_base as sqla_declarative_base
import logging
import datetime

_session = None

Base = sqla_declarative_base()

class Group(Base):
    """Used to declare an organization of people"""
    __tablename__ = "groups"
    id = sqla.Column("id", sqla.Integer, primary_key=True)
    name = sqla.Column("name", sqla.String, index=True)
    db_time_added = sqla.Column("db_time_added", sqla.TIMESTAMP)

class GroupHIST(Base):
    """Used to declare an organization of people, but for historical data"""
    __tablename__ = "groups_HIST"
    id = sqla.Column("id", sqla.Integer, primary_key=True)
    name = sqla.Column("name", sqla.String, index=True)
    db_time_added = sqla.Column("db_time_added", sqla.TIMESTAMP)

class Instance(Base):
    """Used to declare an instance of a service"""
    __tablename__ = "instances"
    id = sqla.Column("id", sqla.Integer, primary_key=True)
    servicetype = sqla.Column("servicetype", sqla.String)
    name = sqla.Column("name", sqla.String)
    db_time_added = sqla.Column("db_time_added", sqla.TIMESTAMP)

    index_instances = sqla.Index("servicetype", "name", unique=True)

class InstanceHIST(Base):
    """Used to declare an instance of a service, but for historical data"""
    __tablename__ = "instances_HIST"
    id = sqla.Column("id", sqla.Integer, primary_key=True)
    servicetype = sqla.Column("servicetype", sqla.String)
    name = sqla.Column("name", sqla.String)
    db_time_added = sqla.Column("db_time_added", sqla.TIMESTAMP)

    index_instances_HIST = sqla.Index("servicetype", "name", unique=True)

class Account(Base):
    """Used to declare an account on a service"""
    __tablename__ = "accounts"
    id = sqla.Column("id", sqla.Integer, primary_key=True)
    id_serviceinstance = sqla.Column("id_serviceinstance", sqla.ForeignKey('instances.id'))
    db_time_added = sqla.Column("db_time_added", sqla.TIMESTAMP)
    id_original = sqla.Column("id_original", sqla.String)
    id_address_main = sqla.Column("id_address_main", sqla.ForeignKey('addresses.id'))
    account_type = sqla.Column("account_type", sqla.String)
    suspended = sqla.Column("suspended", sqla.Boolean)
    archived = sqla.Column("archived", sqla.Boolean)
    deleted = sqla.Column("deleted", sqla.Boolean)
    billed = sqla.Column("billed", sqla.Boolean)
    username = sqla.Column("username", sqla.String)
    name = sqla.Column("name", sqla.String)
    name_full = sqla.Column("name_full", sqla.String)
    name_first = sqla.Column("name_first", sqla.String)
    name_last = sqla.Column("name_last", sqla.String)
    time_birth = sqla.Column("time_birth", sqla.TIMESTAMP)
    time_registered = sqla.Column("time_registered", sqla.TIMESTAMP)
    time_login_first = sqla.Column("time_login_first", sqla.TIMESTAMP)
    time_login_earliest = sqla.Column("time_login_earliest", sqla.TIMESTAMP)
    time_login_last = sqla.Column("time_login_last", sqla.TIMESTAMP)
    time_active_last = sqla.Column("time_active_last", sqla.TIMESTAMP)
    auth_db = sqla.Column("auth_db", sqla.String)
    data_location = sqla.Column("data_location", sqla.String)
    notes = sqla.Column("notes", sqla.String)
    messages = sqla.Column("messages", sqla.String)
    days_active = sqla.Column("days_active", sqla.Integer)

    serviceinstance = sqla.orm.relationship('Instance', back_populates='accounts')

    index_accounts = sqla.Index("id_serviceinstance", "id_original", unique=True)

class AccountHIST(Base):
    """Used to declare an account on a service, but for historical data"""
    __tablename__ = "accounts_HIST"
    id = sqla.Column("id", sqla.Integer, primary_key=True)
    id_serviceinstance = sqla.Column("id_serviceinstance", sqla.Integer)
    db_time_added = sqla.Column("db_time_added", sqla.TIMESTAMP)
    id_original = sqla.Column("id_original", sqla.String)
    id_address_main = sqla.Column("id_address_main", sqla.Integer)
    account_type = sqla.Column("account_type", sqla.String)
    suspended = sqla.Column("suspended", sqla.Boolean)
    archived = sqla.Column("archived", sqla.Boolean)
    deleted = sqla.Column("deleted", sqla.Boolean)
    billed = sqla.Column("billed", sqla.Boolean)
    username = sqla.Column("username", sqla.String)
    name = sqla.Column("name", sqla.String)
    name_full = sqla.Column("name_full", sqla.String)
    name_first = sqla.Column("name_first", sqla.String)
    name_last = sqla.Column("name_last", sqla.String)
    time_birth = sqla.Column("time_birth", sqla.TIMESTAMP)
    time_registered = sqla.Column("time_registered", sqla.TIMESTAMP)
    time_login_first = sqla.Column("time_login_first", sqla.TIMESTAMP)
    time_login_earliest = sqla.Column("time_login_earliest", sqla.TIMESTAMP)
    time_login_last = sqla.Column("time_login_last", sqla.TIMESTAMP)
    time_active_last = sqla.Column("time_active_last", sqla.TIMESTAMP)
    auth_db = sqla.Column("auth_db", sqla.String)
    data_location = sqla.Column("data_location", sqla.String)
    notes = sqla.Column("notes", sqla.String)
    messages = sqla.Column("messages", sqla.String)
    days_active = sqla.Column("days_active", sqla.Integer)

    index_accounts_HIST = sqla.Index("id_serviceinstance", "id_original", unique=True)

Instance.accounts = sqla.orm.relationship('Account', order_by=Account.id, back_populates='serviceinstance')

class Addresstype(Base):
    """Used to declare an address type like e-mail or telephone"""
    __tablename__ = "addresstypes"
    id = sqla.Column("id", sqla.Integer, primary_key=True)
    db_time_added = sqla.Column("db_time_added", sqla.TIMESTAMP)
    name = sqla.Column("name", sqla.String)
    description = sqla.Column("description", sqla.String)

class AddresstypeHIST(Base):
    """Used to declare an address type like e-mail or telephone, but for historical data"""
    __tablename__ = "addresstypes_HIST"
    id = sqla.Column("id", sqla.Integer, primary_key=True)
    db_time_added = sqla.Column("db_time_added", sqla.TIMESTAMP)
    name = sqla.Column("name", sqla.String)
    description = sqla.Column("description", sqla.String)

class AccountAddress(Base):
    """Used to declare a mapping between an address and an account"""
    __tablename__ = "accountsaddresses"
    id_account = sqla.Column("id_account", sqla.Integer, sqla.ForeignKey('accounts.id'), primary_key=True)
    id_address = sqla.Column("id_address", sqla.Integer, sqla.ForeignKey('addresses.id'), primary_key=True)

Account.addresses = sqla.orm.relationship('Address', secondary='accountsaddresses', back_populates='accounts')

class AccountAddressHIST(Base):
    """Used to declare a mapping between an address and an account, but for historical data"""
    __tablename__ = "accountsaddresses_HIST"
    id_account = sqla.Column("id_account", sqla.Integer, primary_key=True)
    id_address = sqla.Column("id_address", sqla.Integer, primary_key=True)

class Address(Base):
    """Used to declare an address where a person can be reached"""
    __tablename__ = "addresses"
    id = sqla.Column("id", sqla.Integer, primary_key=True)
    id_person = sqla.Column("id_person", sqla.ForeignKey('persons.id'))
    db_time_added = sqla.Column("db_time_added", sqla.TIMESTAMP)
    id_type = sqla.Column("id_type", sqla.Integer, sqla.ForeignKey('addresstypes.id'))
    uri = sqla.Column("uri", sqla.String)
    uri_original = sqla.Column("uri_original", sqla.String)
    addresstype = sqla.orm.relationship('Addresstype', back_populates='addresses')
    accounts_main = sqla.orm.relationship('Account', back_populates='main_address', foreign_keys="Account.id_address_main")

    accounts = sqla.orm.relationship('Account', secondary='accountsaddresses', back_populates='addresses')

Addresstype.addresses = sqla.orm.relationship('Address', back_populates='addresstype')
Account.main_address = sqla.orm.relationship('Address', back_populates='accounts_main')

class AddressHIST(Base):
    """Used to declare an address where a person can be reached, but for historical data"""
    __tablename__ = "addresses_HIST"
    id = sqla.Column("id", sqla.Integer, primary_key=True)
    id_person = sqla.Column("id_person", sqla.Integer)
    db_time_added = sqla.Column("db_time_added", sqla.TIMESTAMP)
    id_type = sqla.Column("id_type", sqla.Integer)
    uri = sqla.Column("uri", sqla.String)
    uri_original = sqla.Column("uri_original", sqla.String)

class Person(Base):
    """Used to declare a person which has multiple accounts"""
    __tablename__ = "persons"
    id = sqla.Column("id", sqla.Integer, primary_key=True)
    db_time_added = sqla.Column("db_time_added", sqla.TIMESTAMP)
    name_first = sqla.Column("name_first", sqla.String)
    name_last = sqla.Column("name_last", sqla.String)
    time_birth = sqla.Column("time_birth", sqla.TIMESTAMP)
    time_registered = sqla.Column("time_registered", sqla.TIMESTAMP)

    addresses = sqla.orm.relationship('Address', back_populates='person')

Address.person = sqla.orm.relationship('Person', back_populates='addresses')

class PersonHIST(Base):
    """Used to declare a person which has multiple accounts, but for historical data"""
    __tablename__ = "persons_HIST"
    id = sqla.Column(sqla.Integer, primary_key=True)
    db_time_added = sqla.Column("db_time_added", sqla.TIMESTAMP)
    name_first = sqla.Column("name_first", sqla.String)
    name_last = sqla.Column("name_last", sqla.String)
    time_birth = sqla.Column("time_birth", sqla.TIMESTAMP)

class GroupInstance(Base):
    """Used to declare a mapping between a group and an instance"""
    __tablename__ = "groupsinstances"
    id_group = sqla.Column("id_group", sqla.ForeignKey('groups.id'), primary_key=True)
    id_serviceinstance = sqla.Column("id_serviceinstance", sqla.ForeignKey('instances.id'), primary_key=True)

class GroupInstanceHIST(Base):
    """Used to declare a mapping between a group and an instance, but for historical data"""
    __tablename__ = "groupsinstances_HIST"
    id_group = sqla.Column("id_group", sqla.Integer, primary_key=True)
    id_serviceinstance = sqla.Column("id_serviceinstance", sqla.Integer, primary_key=True)

Group.instances = sqla.orm.relationship('Instance', secondary='groupsinstances', back_populates='groups')
Instance.groups = sqla.orm.relationship('Group', secondary='groupsinstances', back_populates='instances')

class GroupPerson(Base):
    """Used to declare a mapping between a group and a person"""
    __tablename__ = "groupspersons"
    id_group = sqla.Column("id_group", sqla.ForeignKey('groups.id'), primary_key=True)
    id_person = sqla.Column("id_person", sqla.ForeignKey('persons.id'), primary_key=True)

class GroupPersonHIST(Base):
    """Used to declare a mapping between a group and a person, but for historical data"""
    __tablename__ = "groupspersons_HIST"
    id_group = sqla.Column("id_group", sqla.Integer, primary_key=True)
    id_person = sqla.Column("id_person", sqla.Integer, primary_key=True)

Group.persons = sqla.orm.relationship('Person', secondary='groupspersons', back_populates='groups')
Person.groups = sqla.orm.relationship('Group', secondary='groupspersons', back_populates='persons')

def init_session(uri, echo_commands=False):
    global _session
    engine = sqla.create_engine(uri, echo=echo_commands)
    Base.metadata.create_all(engine)
    Session = sqla.orm.sessionmaker(bind=engine)
    _session = Session()
    return _session

def upsert(model, filters, values={}):
    global _session
    # find entry
    db_Model = _session.query(model).filter_by(**filters).first()
    if db_Model:
        # update entry
        logging.debug(f"{model.__name__} {filters} is known.")
        # for model in Base._decl_class_registry.values():
        #     print(model.__tablename__)
        #     if model.__tablename__ == f"{db_Model.__tablename__}_HIST":
        #         model_HIST = model
        #         break
        # d_Model_HIST = model_HIST()
        # FIXME: implement copy
        # FIXME: implement changing singular values
        for key, value in values.items():
            setattr(db_Model, key, value)
    else:
        # create entry
        logging.debug(f"{model.__name__} {filters} is new.")
        finalvalues = dict(filters)
        finalvalues.update(values)
        finalvalues["db_time_added"] = datetime.datetime.now()
        db_Model = model(**finalvalues)
        _session.add(db_Model)
    # save data
    _session.commit()
    return db_Model
