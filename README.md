# User Index

List all your users across different services, database agnostic.

## Contents
* [Getting Started](#getting-started)
*    [Prerequisites](#prerequisites)
*    [Installing](#installing)
* [Deployment](#deployment)
* [Usage](#usage)
* [Built With](#built-with)
* [Contributing](#contributing)
* [Versioning](#versioning)
* [Authors](#authors)
* [License](#license)
* [Project History](#project-history)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

* Python 3
* Internet

### Installing

A step by step series of examples that tell you how to get a development env running

Say what the step will be

I recommend you to use the `setup_development.sh` script by running

`chmod +x setup_development.sh && ./setup_development.sh`

## Deployment

Copy the secrets.yml.tmpl and fill it in with your login information about your services:
`cp secrets.yml.tmpl secrets.yml && vim secrets.yml`

## Usage

1. Activate environment
`source venv/bin/activate`
2. Gather information
`./main.py --toDB 'sqlite:///userindex-ORGANAME.db' fetch`
3. Associate information
`./main.py --toDB 'sqlite:///userindex-ORGANAME.db' relate`
4. List stray accounts
`./main.py --toDB 'sqlite:///userindex-ORGANAME.db' strays`

## Built With

* [Sublime Text 4](https://www.sublimetext.com/) - The code editor I use
* [Firefox](https://www.mozilla.org/de/firefox/) - The web browser I use

## Contributing

Please [open an issue](https://gitlab.com/MaxValue/user-index/-/issues/new) if you want to help.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/MaxValue/user-index/-/tags).

## Authors

* **Max Fuxjäger** - *Initial work* - [MaxValue](https://gitlab.com/MaxValue/)

## License

This project is licensed under the MIT License - see the [LICENSE.txt](LICENSE.txt) file for details.

## Project History
This project was created because I (Max) needed a script the index users of an organization in order to deduplicate accounts and filter out old members.