#!/usr/bin/env python3
# coding: utf-8

import argparse
import base64
import datetime
import dbSchema
import hashlib
import jmespath
import json
import logging
import oauthlib
import pandas
import re
import requests
import requests_oauthlib
import ruamel.yaml
import urllib
import webbrowser

yaml = ruamel.yaml.YAML()

def editSecretsInstance(secretsFileName, serviceType, serviceInstance, **kwargs):
    global services
    for service in services:
        if service["type"] == serviceType and service["instance"] == serviceInstance:
            for key, value in kwargs.items():
                service[key] = value
            break
    with open(secretsFileName, "w", encoding="utf-8") as secretsFile:
        yaml.dump(services, secretsFile)

def genTokenUpdater(secretsFileName, serviceType, serviceInstance):
    def tokenUpdater(token):
        editSecretsInstance(
            secretsFileName=secretsFileName,
            serviceType=serviceType,
            serviceInstance=serviceInstance,
            access_token=token["access_token"],
            refresh_token=token["refresh_token"],
            expires_at=token["expires_at"],
            token_type=token["token_type"],
        )
    return tokenUpdater

def cleanEmail(emails):
    domainmap = {
        "googlemail.com": "gmail.com",
        "hotmail.com": "outlook.com",
        "hotmail.nl": "outlook.com",
        "live.com": "outlook.com",
        "msn.com": "outlook.com",
        "passport.com": "outlook.com",
        "passport.net": "outlook.com",
        "me.com": "icloud.com",
        "mac.com": "icloud.com",
        "pm.me": "protonmail.com",
        "protonmail.ch": "protonmail.com",
        "tutanota.com": "tutanota.de",
        "tutamail.com": "tutanota.de",
        "tuta.io": "tutanota.de",
        "keemail.me": "tutanota.de",
        "aim.com": "aol.com",
    }
    providers_aliasPlus = [
        "gmail.com",
        "outlook.com",
        "icloud.com",
        "protonmail.com",
        "protonmail.com",
        "mailbox.org",
    ]
    providers_aliasMinus = [
        "yahoo.com",
    ]
    providers_aliasDots = [
        "gmail.com",
    ]
    re_email = re.compile(r"^(?P<user>[^@]+)@(?P<domain>[^@]+)$", re.IGNORECASE)
    re_aliasPlus = re.compile(r"^(?P<user>[^@\+]+)(\+(?P<suffix>[^@\+]+))?@(?P<domain>[^@]+)$", re.IGNORECASE)
    re_aliasMinus = re.compile(r"^(?P<user>[^@-]+)(-(?P<suffix>[^@-]+))?@(?P<domain>[^@]+)$", re.IGNORECASE)

    result = []

    for rawEmail in emails:
        emailStr = rawEmail.lower()
        email = re_email.match(emailStr)
        if email.group("domain") in domainmap:
            email_domain = domainmap[email.group("domain")]
            emailStr = f"{email.group('user')}@{email_domain}"
            email = re_email.match(emailStr)
        if email.group("domain") in providers_aliasPlus:
            email = re_aliasPlus.match(emailStr)
            emailStr = f"{email.group('user')}@{email.group('domain')}"
            email = re_email.match(emailStr)
        if email.group("domain") in providers_aliasMinus:
            email = re_aliasMinus.match(emailStr)
            emailStr = f"{email.group('user')}@{email.group('domain')}"
            email = re_email.match(emailStr)
        if email.group("domain") in providers_aliasDots:
            user = email.group("user").replace(".", "")
            emailStr = f"{user}@{email.group('domain')}"
            email = re_email.match(emailStr)
        result.append(emailStr)
    return result

def chooser(options):
    options = ["Skip"]+list(names_first)
    option = -1
    while option < 0 or option >= len(options):
        logging.warning("########################\n")
        for i, name_first in enumerate(options):
            logging.warning(f"[{i}] {name_first!r}")
        option = int(input("Choose option: "))
    return option

def defVal(var, noneVal=None, default=None):
    if var is noneVal:
        return default
    return var

def insertAddress(addrType, address):
    if address in ["", None]:
        return None
    if addrType == "email":
        cleanedAddress = cleanEmail([address])[0]
    elif addrType == "phone":
        cleanedAddress = address
    elif addrType == "postal":
        cleanedAddress = address
    else:
        cleanedAddress = address
    DBaddresstype = dbSchema.upsert(
        model = dbSchema.Addresstype,
        filters = {
            "name": addrType,
        },
    )
    DBaddress = dbSchema.upsert(
        model = dbSchema.Address,
        filters = {
            "addresstype": DBaddresstype,
            "uri_original": address,
            "uri": cleanedAddress,
        },
    )
    return DBaddress

def fetch(secrets, sources="all", dbURI=None):
    if dbURI:
        dbConnection = dbSchema.init_session(uri=dbURI)
    for service in services:
        logging.info(f"Fetching data for service {service['type']!r}:{service['instance']!r}")
        if dbURI:
            DBservice = dbSchema.upsert(
                model=dbSchema.Instance,
                filters={
                    "servicetype": service["type"],
                    "name": service["instance"],
                },
            )
            DBgroups = []
            for group in service["groups"]:
                DBgroup = dbSchema.upsert(
                    model=dbSchema.Group,
                    filters={
                        "name": group,
                    },
                )
                DBgroup.instances.append(DBservice)
                DBgroups.append(DBgroup)
            dbConnection.commit()
        if "all" in args.sources or service.get("instance", "") in args.sources:
            if service.get("type") == "craftcms":
                response = requests.post(service["apiurl"],
                    json={
                        "query": """
                            query {
                              users {
                                uid,
                                enabled,
                                archived,
                                trashed,
                                dateCreated @formatDateTime (format: "Y-m-d H:i:s.ue"),
                                friendlyName,
                                fullName,
                                name,
                                username,
                                firstName,
                                lastName,
                                email,
                              }
                            }
                        """
                    },
                    headers={
                        "Authorization": f"Bearer {service['token']}"
                    }
                )
                for userData in response.json().get("data", {}).get("users", []):
                    if userData["enabled"]:
                        userData_suspended = False
                    else:
                        userData_suspended = True
                    if dbURI:
                        dbEmail = insertAddress("email", userData["email"])
                        dbSchema.upsert(
                            model=dbSchema.Account,
                            filters={
                                "id_original": userData.get("uid"),
                                "serviceinstance": DBservice,
                            },
                            values={
                                "main_address": dbEmail,
                                "suspended": userData_suspended,
                                "archived": userData["archived"],
                                "deleted": userData["trashed"],
                                "username": userData["username"],
                                "name": userData["name"],
                                "name_full": userData["fullName"],
                                "name_first": userData["firstName"],
                                "name_last": userData["lastName"],
                                "time_registered": datetime.datetime.strptime(userData["dateCreated"], "%Y-%m-%d %H:%M:%S.%f%z"),
                            },
                        )
            elif service.get("type") == "excel":
                sheetDSGVO = pandas.read_excel(service["file"], service["sheet"], index_col=None, na_values=["NA"])
                for userData in sheetDSGVO.to_numpy():
                    email_original = userData[service.get("col_emailAddress")]
                    time_dsgvo = userData[service.get("col_time_dsgvo")]
                    notes = userData[service.get("col_notes")]
                    contractLocation = userData[service.get("col_contractLocation")]
                    if dbURI:
                        dbEmail = insertAddress("email", email_original)
                        dbSchema.upsert(
                            model=dbSchema.Account,
                            filters={
                                "id_original": email_original,
                                "serviceinstance": DBservice,
                            },
                            values={
                                "main_address": dbEmail,
                                "time_registered": time_dsgvo,
                                "data_location": contractLocation,
                                "notes": notes,
                            },
                        )
            elif service.get("type") == "googleworkspace":
                tokenUpdater = genTokenUpdater(args.secrets, serviceType=service["type"], serviceInstance=service["instance"])
                if not set(("access_token", "expires_at", "token_type", "refresh_token")) <= set(service):
                    oauth = requests_oauthlib.OAuth2Session(
                        service["client_id"],
                        scope=["https://www.googleapis.com/auth/admin.directory.user.readonly"],
                        redirect_uri="https://localhost",
                    )
                    authorization_url, state = oauth.authorization_url(
                        "https://accounts.google.com/o/oauth2/auth",
                        access_type="offline",
                        prompt="select_account"
                    )
                    logging.warning(f"Token not defined! Authorize App with the page in your browser or open this URL manually: {authorization_url}")
                    webbrowser.open_new_tab(authorization_url)
                    redirect_response = input("Paste the full response URL here: ")
                    oauth = requests_oauthlib.OAuth2Session(
                        service["client_id"],
                        redirect_uri="https://localhost",
                        state=state,
                    )
                    tokenUpdater(
                        oauth.fetch_token(
                            "https://oauth2.googleapis.com/token",
                            client_secret=service["client_secret"],
                            authorization_response=redirect_response,
                        )
                    )
                oauth = requests_oauthlib.OAuth2Session(
                    service["client_id"],
                    token={
                        "access_token": service["access_token"],
                        "expires_at": service["expires_at"],
                        "token_type": service["token_type"],
                        "refresh_token": service["refresh_token"],
                    },
                    auto_refresh_kwargs={
                        "client_id": service["client_id"],
                        "client_secret": service["client_secret"],
                    },
                    auto_refresh_url="https://oauth2.googleapis.com/token",
                    token_updater=tokenUpdater,
                )
                normalUsers = oauth.get('https://admin.googleapis.com/admin/directory/v1/users',
                    params={
                        "domain": service["domain"],
                        "maxResults": 500,
                    }
                ).json().get("users", [])
                deletedUsers = oauth.get('https://admin.googleapis.com/admin/directory/v1/users',
                    params={
                        "domain": service["domain"],
                        "maxResults": 500,
                        "showDeleted": "true",
                    }
                ).json().get("users", [])
                if dbURI:
                    for userData in normalUsers+deletedUsers:
                        if userData["isAdmin"]:
                            userData_account_type = "Admin"
                        elif userData["isDelegatedAdmin"]:
                            userData_account_type = "Delegated Admin"
                        else:
                            userData_account_type = "User"
                        dbEmail = insertAddress("email", userData["primaryEmail"])
                        dbAccount = dbSchema.upsert(
                            model=dbSchema.Account,
                            filters={
                                "id_original": userData["id"],
                                "serviceinstance": DBservice,
                            },
                            values={
                                "account_type": userData_account_type,
                                "main_address": dbEmail,
                                "suspended": userData["suspended"],
                                "archived": userData["archived"],
                                "name": userData["name"]["fullName"],
                                "name_first": userData["name"]["givenName"],
                                "name_last": userData["name"]["familyName"],
                                "time_registered": datetime.datetime.strptime(userData["creationTime"], "%Y-%m-%dT%H:%M:%S.%f%z"),
                                "time_login_last": datetime.datetime.strptime(userData["lastLoginTime"], "%Y-%m-%dT%H:%M:%S.%f%z"),
                                "auth_db": userData["kind"],
                            },
                        )
                        for email_etc in jmespath.search("emails[].address", userData):
                            dbAccount.addresses.append(insertAddress("email", email_etc))
                            dbConnection.commit()
            elif service.get("type") == "kirbycms":
                response = requests.get(f"https://{service['api_url']}/users",
                    auth=(
                        service['email'],
                        service['password'],
                    ),
                    params={
                        "select": "email,id,name,username,roles"
                    },
                )
                for userData in response.json().get("data", []):
                    if dbURI:
                        dbEmail = insertAddress("email", userData.get("email"))
                        dbSchema.upsert(
                            model=dbSchema.Account,
                            filters={
                                "id_original": userData.get("id"),
                                "serviceinstance": DBservice,
                            },
                            values={
                                "account_type": ",".join(jmespath.search("roles[].title", userData)),
                                "main_address": dbEmail,
                                "name": userData.get("name"),
                                "username": userData.get("username"),
                            },
                        )
            elif service.get("type") == "limesurvey":
                response = requests.post(service["rpc_url"],
                    json = {
                        "method": "get_session_key",
                        "params": {
                            "username": service["username"],
                            "password": service["password"],
                        },
                        "id": 1,
                    },
                )
                sessionID = response.json()["result"]
                response = requests.post(service["rpc_url"],
                    json = {
                        "method": "list_users",
                        "id": 2,
                        "params": {
                            "sSessionKey": sessionID,
                        },
                    },
                )
                for userData in response.json()["result"]:
                    last_login = jmespath.search("last_login", userData)
                    if last_login:
                        last_login = datetime.datetime.strptime(last_login, "%Y-%m-%d %H:%M:%S")
                    if filter(lambda x: x["permission"] == "superadmin", userData["permissions"]):
                        userData_account_type = "Superadmin"
                    else:
                        userData_account_type = "User"
                    if dbURI:
                        dbEmail = insertAddress("email", jmespath.search("email", userData))
                        dbUser = dbSchema.upsert(
                            model=dbSchema.Account,
                            filters={
                                "id_original": jmespath.search("uid", userData),
                                "serviceinstance": DBservice,
                            },
                            values={
                                "account_type": userData_account_type,
                                "main_address": dbEmail,
                                "username": jmespath.search("users_name", userData),
                                "name_full": jmespath.search("full_name", userData),
                                "time_registered": datetime.datetime.strptime(jmespath.search("created", userData), "%Y-%m-%d %H:%M:%S.%f"),
                                "time_login_last": last_login,
                            },
                        )
                requests.post(service["rpc_url"],
                    headers = {
                        "Content-type": "application/json",
                    },
                    json = {
                        "method": "release_session_key",
                        "id": 3,
                        "params": {
                            "sSessionKey": sessionID,
                        },
                    },
                )
            elif service.get("type") in ["matomo", "piwik"]:
                response = requests.get(f"https://{service['server_uri']}",
                    params = {
                        "module": "API",
                        "method": "UsersManager.getUsers",
                        "userLogin": "maxvalue",
                        "format": "JSON",
                        "token_auth": service["token"],
                    },
                )
                for userData in response.json():
                    if userData["superuser_access"] == "1":
                        userData_account_type = "Superuser"
                    else:
                        userData_account_type = "User"
                    last_login = jmespath.search("last_seen", userData)
                    if last_login:
                        last_login = datetime.datetime.strptime(last_login, "%Y-%m-%d %H:%M:%S")
                    if dbURI:
                        dbEmail = insertAddress("email", jmespath.search("email", userData))
                        dbUser = dbSchema.upsert(
                            model=dbSchema.Account,
                            filters={
                                "id_original": jmespath.search("login", userData),
                                "serviceinstance": DBservice,
                            },
                            values={
                                "account_type": userData_account_type,
                                "main_address": dbEmail,
                                "username": jmespath.search("users_name", userData),
                                "name": jmespath.search("full_name", userData),
                                "time_registered": datetime.datetime.strptime(jmespath.search("date_registered", userData), "%Y-%m-%d %H:%M:%S"),
                                "time_login_last": last_login,
                            },
                        )
            elif service.get("type") == "nextcloud":
                response = requests.get(urllib.parse.urljoin(service["server"],"ocs/v1.php/cloud/users"),
                    headers = {
                        "Accept": "application/json",
                        "OCS-APIRequest": "true",
                    },
                    auth = (
                        service["username"],
                        service["password"],
                    ),
                )
                userIDs = jmespath.search("ocs.data.users", response.json())
                for userID in userIDs:
                    response = requests.get(urllib.parse.urljoin(service["server"],f"ocs/v1.php/cloud/users/{userID}"),
                        headers = {
                            "Accept": "application/json",
                            "OCS-APIRequest": "true",
                        },
                        auth = (
                            service["username"],
                            service["password"],
                        ),
                    )
                    userData = jmespath.search("ocs.data", response.json())
                    if "admin" in userData["groups"]:
                        userData_account_type = "Admin"
                    elif len(userData["subadmin"]) > 0:
                        userData_account_type = "Delegated Admin"
                    else:
                        userData_account_type = "User"
                    if userData["enabled"]:
                        userData_suspended = False
                    else:
                        userData_suspended = True
                    if dbURI:
                        dbEmail = insertAddress("email", jmespath.search("email", userData))
                        dbAccount = dbSchema.upsert(
                            model=dbSchema.Account,
                            filters={
                                "id_original": jmespath.search("id", userData),
                                "serviceinstance": DBservice,
                            },
                            values={
                                "account_type": userData_account_type,
                                "main_address": dbEmail,
                                "suspended": userData_suspended,
                                "name": jmespath.search("displayname", userData),
                                "time_login_last": datetime.datetime.fromtimestamp(jmespath.search("lastLogin", userData)/1000),
                                "auth_db": jmespath.search("backend", userData),
                                "data_location": jmespath.search("storageLocation", userData),
                            },
                        )
                        for email_etc in userData["additional_mail"]:
                            dbAccount.addresses.append(insertAddress("email", email_etc))
                        dbConnection.commit()
            elif service.get("type") == "outline":
                reachedEnd = False
                next_page = 0
                users = []
                while not reachedEnd:
                    response = requests.post(f"https://{service['domain']}/api/users.list",
                        headers={
                            "Authorization": f"Bearer {service['token']}"
                        },  
                        json = {
                            "offset": next_page,
                            "limit": 100,
                            "filter": "all",
                        },
                    )
                    payload = response.json()
                    users += jmespath.search("data", payload)
                    current_page = jmespath.search(f"pagination.offset", payload)
                    if not current_page:
                        current_page = 0
                    max_page = jmespath.search(f"pagination.total", payload)
                    if not max_page:
                        max_page = 0
                    next_page = False
                    if current_page < max_page:
                        next_page = current_page + 100
                    elif next_page:
                        logging.debug(f"Next page is {next_page}")
                    else:
                        logging.debug(f"Reached end!")
                        reachedEnd = True
                for userData in users:
                    if userData["isAdmin"]:
                        userData_account_type = "Admin"
                    elif userData["isViewer"]:
                        userData_account_type = "Viewer"
                    else:
                        userData_account_type = "User"
                    last_login = jmespath.search("lastActiveAt", userData)
                    if last_login:
                        last_login = datetime.datetime.strptime(last_login, "%Y-%m-%dT%H:%M:%S.%f%z")
                    dbEmail = insertAddress("email", jmespath.search("email", userData))
                    dbAccount = dbSchema.upsert(
                        model=dbSchema.Account,
                        filters={
                            "id_original": jmespath.search("id", userData),
                            "serviceinstance": DBservice,
                        },
                        values={
                            "account_type": userData_account_type,
                            "main_address": dbEmail,
                            "suspended": jmespath.search("isSuspended", userData),
                            "name": jmespath.search("name", userData),
                            "time_registered": datetime.datetime.strptime(jmespath.search("createdAt", userData), "%Y-%m-%dT%H:%M:%S.%f%z"),
                            "time_login_last": last_login,
                        },
                    )
            elif service.get("type") == "suitecrm_users":
                response = requests.post(service["apiurl"],
                    data={
                        "method": "login",
                        "input_type": "JSON",
                        "response_type": "JSON",
                        "rest_data": json.dumps({
                            "user_auth": {
                                "user_name": service["user_name"],
                                "password": hashlib.md5(bytes(service["password"], "utf-8")).hexdigest(),
                            },
                            "application_name": "User-Index",
                            "name_value_list": {}
                        })
                    }
                )
                sessionID = response.json()["id"]
                response = requests.post(service["apiurl"],
                    data={
                        "method": "get_entry_list",
                        "input_type": "JSON",
                        "response_type": "JSON",
                        "rest_data": json.dumps({
                            "session": sessionID,
                            "module_name": "Users",
                            "max_results": 2,
                        })
                    }
                )
                for userData in response.json().get("entry_list", []):
                    if jmespath.search("name_value_list.is_admin.value", userData) == "Active":
                        userData_account_type = "Admin"
                    else:
                        userData_account_type = "User"
                    if jmespath.search("name_value_list.status.value", userData) == "Active":
                        userData_suspended = False
                        userData_archived = False
                    else:
                        userData_suspended = True
                        userData_archived = True
                    if jmespath.search("name_value_list.external_auth_only.value", userData) == "1":
                        userData_auth_db = "external"
                    else:
                        userData_auth_db = "local"
                    if dbURI:
                        dbEmail = insertAddress("email", jmespath.search("name_value_list.email1.value", userData))
                        dbAccount = dbSchema.upsert(
                            model=dbSchema.Account,
                            filters={
                                "id_original": jmespath.search("id", userData),
                                "serviceinstance": DBservice,
                            },
                            values={
                                "account_type": userData_account_type,
                                "main_address": dbEmail,
                                "deleted": int(jmespath.search("name_value_list.deleted.value", userData)),
                                "username": jmespath.search("name_value_list.user_name.value", userData),
                                "name": jmespath.search("name_value_list.name.value", userData),
                                "name_full": jmespath.search("name_value_list.full_name.value", userData),
                                "name_first": jmespath.search("name_value_list.first_name.value", userData),
                                "name_last": jmespath.search("name_value_list.last_name.value", userData),
                                "time_registered": datetime.datetime.strptime(jmespath.search("name_value_list.date_entered.value", userData), "%Y-%m-%d %H:%M:%S"),
                                "auth_db": userData_auth_db,
                                "notes": jmespath.search("name_value_list.description.value", userData),
                            },
                        )
            elif service.get("type") == "trello":
                response = requests.get(f"https://api.trello.com/1/organizations/{service['workspace']}/members",
                    params={
                        "key": service["key"],
                        "token": service["token"],
                    },
                )
                for member in response.json():
                    response = requests.get(f"https://api.trello.com/1/members/{member['id']}",
                        params={
                            "key": service["key"],
                            "token": service["token"],
                            "fields": "all",
                        },
                    )
                    userData = response.json()
                    if dbURI:
                        dbEmail = insertAddress("email", userData.get("email"))
                        dbAccount = dbSchema.upsert(
                            model=dbSchema.Account,
                            filters={
                                "id_original": jmespath.search("id", userData),
                                "serviceinstance": DBservice,
                            },
                            values={
                                "account_type": userData.get("memberType"),
                                "main_address": dbEmail,
                                "username": userData.get("username"),
                                "name_full": userData.get("fullName"),
                                "notes": userData.get("bio"),
                            },
                        )
            else:
                logging.warning(f"The service {service.get('instance')!r} of type {service.get('type')!r} is unknown! Please open an issue to request this service.")
        else:
            logging.info(f"Skipping instance {service.get('instance')!r} of type {service.get('type')!r}")

def relate(dbURI):
    dbConnection = dbSchema.init_session(uri=dbURI)

    dbAddressesWithoutPerson = dbConnection.query(
        dbSchema.Address
    ).filter(
        dbSchema.Address.id_person.is_(None)
    ).all()
    for addressWithoutPerson in dbAddressesWithoutPerson:
        names_first = set()
        names_last = set()
        for account in addressWithoutPerson.accounts:
            names_first.add(account.name_first)
            names_last.add(account.name_last)
        names_first.discard(None)
        names_first.discard("")
        names_last.discard(None)
        names_last.discard("")
        names_first = list(names_first)
        names_last = list(names_last)
        name_first = None
        name_last = None
        if len(names_first) == 1:
            names_first = names_first[0]
        elif len(names_first) > 1:
            logging.warning("Multiple first names found!")
            option = chooser(names_first)
            if option > 0:
                name_first = names_first[option-1]
        if len(names_last) == 1:
            name_last = names_last[0]
        elif len(names_last) > 1:
            logging.warning("Multiple last names found!")
            option = chooser(names_last)
            if option > 0:
                name_last = names_last[option-1]

        addressWithoutPerson.person = dbSchema.Person(
            name_first = name_first,
            name_last = name_last,
        )
        dbConnection.commit()

    # associate persons with groups
    for service in services:
        if "verifies" in service and len(service["verifies"]) > 0:
            for group in service["verifies"]:
                dbGroup = dbConnection.query(
                    dbSchema.Group,
                ).filter(
                    dbSchema.Group.name.is_(group),
                ).first()
                dbInstance = dbConnection.query(
                    dbSchema.Instance,
                ).filter(
                    dbSchema.Instance.servicetype.is_(service["type"]),
                    dbSchema.Instance.name.is_(service["instance"]),
                ).first()
                for account in dbInstance.accounts:
                    if account.main_address.person:
                        account.main_address.person.groups.append(dbGroup)
                        dbConnection.commit()
                    for address_etc in account.addresses:
                        if address_etc.person:
                            address_etc.person.groups.append(dbGroup)

def strays(dbURI):
    dbConnection = dbSchema.init_session(uri=dbURI)
    dbPersonsWithoutGroup = dbConnection.query(
        dbSchema.Person,
    ).filter(
        ~dbSchema.Person.groups.any(),
    ).all()
    logging.warning("These persons do not belong to a group:")
    for person in dbPersonsWithoutGroup:
        logging.warning(f"{person.name_first} {person.name_last}")
        for address in person.addresses:
            logging.warning(f"\t{address.uri_original}")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(argument_default=False, description="Gather user data.")
    parser.add_argument("--verbose", "-v", action="count", default=0, help="Turn on verbose mode.")
    parser.add_argument("--quiet", "-q", action="store_true", help="Only return data but no log messages.")
    parser.add_argument("--log", help="Logfile path. If omitted, stdout is used.")
    parser.add_argument("--debug", "-d", action="store_true", help="Log all messages including debug.")
    parser.add_argument("--secrets", default="secrets.yml", help="The file which holds the login informations for the sources.")
    parser.add_argument("--sources", default="all", help="The sources from which to gather the data.")
    parser.add_argument("--toDB", default=None, help="The sqlite database file to use.")
    parser.add_argument("mode", choices=["fetch","relate", "strays"], help="What to do with the users.")
    args = parser.parse_args()

    if args.quiet:
        loglevel = 100
    elif args.debug:
        loglevel = logging.DEBUG
    elif args.verbose:
        loglevel = logging.INFO
    else:
        loglevel = logging.WARNING

    if args.log:
        logging.basicConfig(filename=args.log, filemode="a", level=loglevel)
    else:
        logging.basicConfig(level=loglevel)

    args.sources = set(args.sources.split(","))

    # Load login information
    with open(args.secrets, "r", encoding="utf-8") as secretsFile:
        services = yaml.load(secretsFile)

    if args.mode == "fetch":
        fetch(
            secrets=services,
            sources=args.sources,
            dbURI=args.toDB,
        )
    elif args.mode == "relate":
        relate(dbURI=args.toDB)
    elif args.mode == "strays":
        strays(dbURI=args.toDB)
